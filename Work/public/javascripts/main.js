$(document).ready(function(){
	var currency_arr = [];
	var basePrice = $('.span-value').html();
	$.ajax({
		type: 'GET',
		url: 'http://localhost:80/currencies',
		dataType: 'json',
		success: function (data) { //success getting currency conversion.
			var result = data;
			currency_arr=data.getallResult.diffgram.DocumentElement.Currency;
			var usd = currency_arr.find(x=>x.IDMoneda == "USD");
			usd.Value *= basePrice;
			$('#currencies').change(function() {
				let chosenVal = $(this).val();
				for(el of currency_arr) {
					if(chosenVal != "USD") {
						if(el.IDMoneda == chosenVal && el.IDMoneda != "USD"){
							$('.span-value').html((usd.Value/el.Value).toFixed(2));
						}	
					}
					else {
						$('.span-value').html(basePrice);
					}
				}
			})
		},
		error: function (data) { // error getting data from bnr.
			console.log("Error.", data);
		}
	});
})