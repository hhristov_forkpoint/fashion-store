function serverdata () {
	this.PROTOCOL = 'http';
    this.WEB_HOST = 'localhost';
    this.WEB_PORT = 80;
    this.URL = this.PROTOCOL + '://' + this.WEB_HOST + ':' + this.WEB_PORT;
}