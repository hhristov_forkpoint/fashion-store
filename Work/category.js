var express = require('express');
var router = express.Router();

router.get('/category/:cat/:subcat/:title', function(req, res, next) {
  var _         = require("underscore");
	var db = req.db;
	var mycol = db.collection('categories');
	mycol.find({}, function(err, items) {
		mycol.find({id:req.params.cat}, function(err, products) {
			res.render("category", { 
				_     : _, 
				menu_items: items,
				products: products,
				subcategory: req.params.subcat,
				title: req.params.title,

module.exports = router;

