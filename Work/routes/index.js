var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  var _ = require("underscore");
	var db = req.db;
	var mycol = db.collection('categories');
	mycol.find({}, function(err, items) {
		res.render("index", { 
			_     : _, 
			title : "Fashion store",
			menu_items : items
		});	
	});
});

module.exports = router;
