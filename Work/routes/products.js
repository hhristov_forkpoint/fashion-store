var express = require('express');
var router = express.Router();

router.get('/products/:parent_cat/:product_cat/:title', function(req, res, next) {
	var _         = require("underscore");
	var db = req.db;
	var mycol = db.collection('products');
	var menuCol = db.collection('categories');
	menuCol.find({}, function(err, items) {
		mycol.find({primary_category_id:req.params.product_cat}, function(err,products) {
			res.render("products", { 
				_     : _, 
				menu_items: items,
				products: products,
				title: req.params.title,
				parent: req.params.parent_cat,
				breadcrumb: req.params.product_cat
				});	
			});
		});
	});

module.exports = router;