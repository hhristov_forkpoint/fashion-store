var express = require('express');
var router = express.Router();


router.get('/currencies', function(req, res, next) {
	var soap = require("soap");
	var url = 'http://infovalutar.ro/curs.asmx?wsdl';
	var args = {
	  dt:'2017-08-09T00:00:00'
	};
	var _         = require("underscore");
	soap.createClient(url, function(err, client) {
		client.getall(args, function(err, result) {
			res.json(result);
		});
	});
});

module.exports = router;