var express = require('express');
var router = express.Router();

router.get('/product/:product_parent_id/:item_name', function(req, res, next) {
	var _         = require("underscore");
	var db = req.db;
	var mycol = db.collection('products');
	var menuCol = db.collection('categories');
	menuCol.find({}, function(err, items) {
		mycol.find({name:req.params.item_name}, function(err, product) {
			res.render("product", { 
				// Underscore.js lib
				_     : _, 
				// Template data
				menu_items: items,
				products: product,
				title: req.params.item_name,
				itemName: req.params.item_name,
				parent: req.params.product_parent_id
			});	
		});
	})
});

module.exports = router;