exports.index = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	mdbClient.connect("mongodb://localhost:27017/Db", function(err, db) {
		var mycol = db.collection('categories');
		mycol.find().toArray(function(err, items) {
				res.render("index", { 
					// Underscore.js lib
					_     : _, 
					// Template data
					title : "Fashion store",
					menu_items : items
				});	
			db.close();
		});
	});
};

exports.currencies = function(req, res) {
	var soap = require("soap");
	var url = 'http://infovalutar.ro/curs.asmx?wsdl';
	var args = {
	  dt:'2017-08-09T00:00:00'
	};
	var _         = require("underscore");
	soap.createClient(url, function(err, client) {
		client.getall(args, function(err, result) {
			res.json(result);
		})
	})
};

exports.category = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	mdbClient.connect("mongodb://localhost:27017/Db", function(err, db) {
		var mycol = db.collection('categories');
		mycol.find({}).toArray(function(err, items) {
			mycol.find({id:req.params.cat}).toArray(function(err, products) {
				res.render("category", { 
					// Underscore.js lib
					_     : _, 
					// Template data
					menu_items: items,
					products: products,
					subcategory: req.params.subcat,
					title: req.params.title,
				});	
			db.close();
			})
		});
	});
}

exports.products = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	mdbClient.connect("mongodb://localhost:27017/Db", function(err, db) {
		var mycol = db.collection('products');
		var menuCol = db.collection('categories');
		menuCol.find({}).toArray(function(err, items) {
			mycol.find({primary_category_id:req.params.product_cat}).toArray(function(err,products) {
				res.render("products", { 
					// Underscore.js lib 
					_     : _, 
					// Template data
					menu_items: items,
					products: products,
					title: req.params.title,
					parent: req.params.parent_cat,
					breadcrumb: req.params.product_cat
				});	
				db.close();
			})
		});
	});
}

exports.product = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	mdbClient.connect("mongodb://localhost:27017/Db", function(err, db) {
		var mycol = db.collection('products');
		var menuCol = db.collection('categories');
		menuCol.find({}).toArray(function(err, items) {
			mycol.find({name:req.params.item_name}).toArray(function(err, product) {
				res.render("product", { 
					// Underscore.js lib
					_     : _, 
					// Template data
					menu_items: items,
					products: product,
					title: req.params.item_name,
					itemName: req.params.item_name,
					parent: req.params.product_parent_id
				});	
				db.close();
			});
		})
	});
}