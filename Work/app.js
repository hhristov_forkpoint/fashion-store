require('dotenv/config');
var express = require('express');
var http = require('http');
var path = require('path');
var mongo = require('mongodb');
var monk = require('monk');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app = express();
var databaseUrl = process.env.DB_HOST + ':' + process.env.DB_PORT + '/' + process.env.DB_NAME;
var db = monk(databaseUrl);
// All environments
app.set("port", process.env.WEB_PORT);
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var index = require('./routes/index');
var category = require('./routes/category');
var products = require('./routes/products');
var product = require('./routes/product');
var currencies = require('./routes/currencies');
app.use(function(req, res, next){
	req.db = db;
	next();
});

app.get('/', index);
app.get('/category/:cat/:subcat/:title', category);
app.get('/products/:parent_cat/:product_cat/:title', products);
app.get('/product/:product_parent_id/:item_name', product);
app.get('/currencies', currencies);

// Run server
http.createServer(app).listen(app.get("port"), function() {
	console.log("Express server listening on port " + app.get("port"));
});

module.exports = app;